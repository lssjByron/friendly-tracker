package com.example.alpha_000.tdprojectam_br;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class Login extends Activity {
    private EditText etUserId = null;
    private EditText etPassId = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void btnLogin(View view) {
        etUserId = (EditText)findViewById(R.id.etUsername);
        etPassId = (EditText)findViewById(R.id.etPassword);
        String usersId = etUserId.getText() + "," + etPassId;
        Intent myIntent = new Intent(Login.this, MainMenu.class);
        myIntent.putExtra("loginId", usersId); //Optional parameters
        Login.this.startActivity(myIntent);
    }
}
